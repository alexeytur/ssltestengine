package engine.test.gosecured.com.testopensslengine;

public class SSLWrapper {

    static
    {
        System.loadLibrary("crypto");
        System.loadLibrary("opensslwrapper");
        System.loadLibrary("ssl");
        System.loadLibrary("simple_engine");
    }

    public native void My_OpenSSL_add_all_algorithms();
    public native void My_SSL_load_error_strings();
    public native String My_SSL_print();
    public native String My_Load_engine();

}
