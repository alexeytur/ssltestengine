#include <stdio.h>
#include <string.h>
#include <android/log.h>

#include <openssl/engine.h>
#include <openssl/crypto.h>
#include <openssl/objects.h>

#define LOG_TAG "SSLEngine"

static const char* engine_id = "simple_engine";
static const char* engine_name = "Test simple engine";

static int bind_fn(ENGINE *e, const char* id)
{
	__android_log_print(ANDROID_LOG_INFO, LOG_TAG, "_____ 11111 simple_engine bind_fn");

	if (!ENGINE_set_id(e, engine_id)) {
		__android_log_print(ANDROID_LOG_INFO, LOG_TAG, "_____ 2222222 simple_engine bind_fn");
        return 0;
    }
    
	if (!ENGINE_set_name(e, engine_name)) {
		__android_log_print(ANDROID_LOG_INFO, LOG_TAG, "_____ 3333333 simple_engine bind_fn");
        return 0;
    }
	__android_log_print(ANDROID_LOG_INFO, LOG_TAG, "_____ 4444444 simple_engine bind_fn");
    return 1;
}

IMPLEMENT_DYNAMIC_BIND_FN (bind_fn)
IMPLEMENT_DYNAMIC_CHECK_FN()
