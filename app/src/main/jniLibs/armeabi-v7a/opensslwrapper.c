#include <string.h>
#include <jni.h>
#include <android/log.h>

#include <openssl/evp.h>
#include <openssl/err.h>

#include <openssl/engine.h>
#include "engine_test_gosecured_com_testopensslengine_SSLWrapper.h"	 

#if __GNUC__ >= 4
    #define DLL_PUBLIC __attribute__ ((visibility ("default")))
    #define DLL_LOCAL  __attribute__ ((visibility ("hidden")))
#else
    #define DLL_PUBLIC
    #define DLL_LOCAL
#endif


#define ENGINE_PATH "/data/data/engine.test.gosecured.com.testopensslengine/lib/"

#define PATH_NAME "/data/data/engine.test.gosecured.com.testopensslengine/lib/libsimple_engine.so"
#define EXPECTED_ENGINE_ID "simple_engine"
#define LOG_TAG "SSLEngine"


JNIEXPORT DLL_PUBLIC void JNICALL Java_engine_test_gosecured_com_testopensslengine_SSLWrapper_My_1OpenSSL_1add_1all_1algorithms
(JNIEnv *env, jobject obj) {
    printf("My_OpenSSL_add_all_algorithms\n");
    return (void)OpenSSL_add_all_algorithms();
}
    
JNIEXPORT DLL_PUBLIC void JNICALL Java_engine_test_gosecured_com_testopensslengine_SSLWrapper_My_1SSL_1load_1error_1strings
(JNIEnv *env, jobject obj) {
    printf("My_SSL_load_error_strings\n");
    return (void)SSL_load_error_strings();
}

JNIEXPORT DLL_PUBLIC jstring JNICALL Java_engine_test_gosecured_com_testopensslengine_SSLWrapper_My_1SSL_1print
(JNIEnv *env, jobject obj) {
    return (*env)->NewStringUTF(env, "My_SSL_print\n");
}

JNIEXPORT DLL_PUBLIC jstring JNICALL Java_engine_test_gosecured_com_testopensslengine_SSLWrapper_My_1Load_1engine
(JNIEnv *env, jobject obj) {
    
	char buf[512];    
	unsigned long err = 0;

    ENGINE_load_dynamic();
	err = ERR_get_error();
    if (err != 0)
    {
		sprintf(buf, "!!! ENGINE_load_dynamic error: %s\n", ERR_error_string(err, NULL));
        __android_log_print(ANDROID_LOG_INFO, LOG_TAG, buf);
    }


    ENGINE * e = ENGINE_by_id( "dynamic" ); //simple_engine
    err = ERR_get_error();
    if (err != 0)
    {
		sprintf(buf, "!!! ENGINE_by_id error: %s\n", ERR_error_string(err, NULL));
        __android_log_print(ANDROID_LOG_INFO, LOG_TAG, buf);
    }

    if (!ENGINE_ctrl_cmd_string(e, "SO_PATH", PATH_NAME, 0))
    {
        __android_log_print(ANDROID_LOG_INFO, LOG_TAG, "SO_PATH error\n");
    }
    if (!ENGINE_ctrl_cmd_string(e, "DIR_ADD", ENGINE_PATH, 0))
    {
        __android_log_print(ANDROID_LOG_INFO, LOG_TAG, "DIR_ADD error\n");
    }
    if (!ENGINE_ctrl_cmd_string(e, "ID", EXPECTED_ENGINE_ID, 0))
    {
        __android_log_print(ANDROID_LOG_INFO, LOG_TAG, "ID error\n");
    }
    if (!ENGINE_ctrl_cmd(e, "LIST_ADD", 1, NULL, NULL, 0))
    {
        __android_log_print(ANDROID_LOG_INFO, LOG_TAG, "LIST_ADD error\n");
    }
    if (!ENGINE_ctrl_cmd_string(e, "LOAD", NULL,0));
    {
        __android_log_print(ANDROID_LOG_INFO, LOG_TAG, "LOAD error\n");
    }

    __android_log_print(ANDROID_LOG_INFO, LOG_TAG, "All going good\n");

    const char *engine_id = ENGINE_get_id(e); //

    __android_log_print(ANDROID_LOG_INFO, LOG_TAG, "engine_id = %s\n", engine_id); // I get engine_id as "dynamic" again!
    ENGINE_set_default(e, ENGINE_METHOD_ALL);

    __android_log_print(ANDROID_LOG_INFO, LOG_TAG, "engine default is set\n");

    ENGINE *myEngine = ENGINE_by_id(EXPECTED_ENGINE_ID);
	
	err = ERR_get_error();
    if (err != 0)
    {
		sprintf(buf, "!!! ENGINE_load_dynamic 22222 error: %s\n", ERR_error_string(err, NULL));
        __android_log_print(ANDROID_LOG_INFO, LOG_TAG, buf);
    }

    if (!myEngine){
        __android_log_print(ANDROID_LOG_INFO, LOG_TAG, "engine not found \n"); //and no engine is found
    }
    if (!ENGINE_init(myEngine)) {
            //ENGINE_finish(myEngine);
            ENGINE_free(myEngine);
            __android_log_print(ANDROID_LOG_INFO, LOG_TAG, "init error \n"); // I finally get init error
	}
    return (*env)->NewStringUTF(env, "My_SSL_print\n");
}
